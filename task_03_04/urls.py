from django.urls import path
from . import views

app_name = "task_03_04"

urlpatterns = [
    path('', views.seeTask03_04, name="task_03_04"),
]