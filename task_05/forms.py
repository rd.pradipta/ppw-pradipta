from django import forms

class MessageForm(forms.Form):
    URGENCY_LIST = (
        ('B', 'Pesan Biasa'),
        ('P', 'Pesan Penting'),
        ('L', 'Pesan Lainnya'),
    )
    sender = forms.CharField(max_length=200, required=True, label='Nama Kamu')
    receiver = forms.CharField(max_length=200, required=True, label='Nama Penerima')
    content = forms.CharField(max_length=1000, required=True, label='Isi Pesan')
    urgency = forms.ChoiceField(required=True, label='Jenis Kepentingan Pesan' , choices=URGENCY_LIST)
