from django.urls import path
from . import views

app_name = "task_05"

urlpatterns = [
    path('', views.seeTask05, name="task_05"),
]
