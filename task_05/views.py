import datetime

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import MessageForm
from .models import DraftPesanContent

def seeTask05(request):
    time_now = datetime.datetime.now()
    if request.method == "POST":
        form = MessageForm(request.POST)
        if form.is_valid():
            sender = request.POST['sender']
            receiver = request.POST['receiver']
            urgency = request.POST['urgency']
            content = request.POST['content']
            data = DraftPesanContent(message_sender=sender, message_receiver=receiver,
                                     message_urgency=urgency, message_content=content,
                                     message_date=time_now)
            data.save()
    else:
        form = MessageForm()

    msg_data = DraftPesanContent.objects.all()
    response = {'form_pesan': form ,
                'semua_pesan': msg_data}
    return render(request, 'draft_pesan.html', response)
