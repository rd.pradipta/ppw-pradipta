import datetime
from django.db import models
from django.utils import timezone

class DraftPesanContent(models.Model):
    URGENCY_LIST = (
        ('B', 'Biasa'),
        ('P', 'Penting'),
        ('L', 'Lainnya'),
    )
    message_sender = models.CharField(max_length=200)
    message_receiver = models.CharField(max_length=200)
    message_content = models.TextField(max_length=1000)
    message_urgency = models.CharField(max_length=1, choices=URGENCY_LIST)
    message_date = models.DateTimeField('Dikirim Pada')

    def __str__(self):
        return  "Pengirim: " + self.message_sender + "\n" + \
                "Penerima: " + self.message_receiver + "\n" + \
                "Kepentingan: " + self.message_urgency + "\n" + \
                "Isi: " + self.message_content + "\n"
