from django.urls import resolve
from django.test import TestCase, Client
from .views import seeTask05
from .models import DraftPesanContent

class Task05Test(TestCase):
    
    def test_task_05_url_exist(self):
        response = Client().get('/task_05/')
        self.assertEqual(response.status_code,200)
    
    def test_task_05_using_template(self):
        response = Client().get('/task_05/')
        self.assertTemplateUsed(response, 'draft_pesan.html')

    def test_task_05_using_function(self):
        found = resolve('/task_05/')
        self.assertEqual(found.func, seeTask05)