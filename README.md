[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/) [![MIT Licence](https://badges.frapsoft.com/os/mit/mit.png?v=103)](https://opensource.org/licenses/mit-license.php) [![pipeline status](https://gitlab.com/rd.pradipta/ppw-pradipta/badges/master/pipeline.svg)](https://gitlab.com/rd.pradipta/ppw-pradipta/commits/master) [![coverage report](https://gitlab.com/rd.pradipta/ppw-pradipta/badges/master/coverage.svg)](https://gitlab.com/rd.pradipta/ppw-dipsi/commits/master)

# Web Design & Programming Tasks Repository - Pradipta Gitaya (Dipsi)
In this repository, you will find my collection of tasks that I have done so far in Web Design & Programming Course in Faculty of Computer Science - Universitas Indonesia

## URL
These tasks & projects can be accessed from [https://ppw-a-dipsi.herokuapp.com](https://ppw-a-dipsi.herokuapp.com)

## Contents
* Task 1 & Task 2 is Not Availabe
* [Task 03 & 04](https://ppw-a-dipsi.herokuapp.com/task_03_04) 
* [Task 05](https://ppw-a-dipsi.herokuapp.com/task_05) 
* [Task 06 & 07](https://ppw-a-dipsi.herokuapp.com/task_06)
* [Task 08](https://ppw-a-dipsi.herokuapp.com/task_08)
* [Task 09](https://ppw-a-dipsi.herokuapp.com/task_09)
* [Task 10](https://ppw-a-dipsi.herokuapp.com/task_10)

## Authors

* **Rd Pradipta Gitaya S** - [Dipta / Dipsi](https://gitlab.com/rd.pradipta)