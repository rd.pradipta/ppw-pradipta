"""Lab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('task_00_homepage.urls')),
	path('task_03_04/', include('task_03_04.urls')),
	path('task_05/', include('task_05.urls')),
    path('task_06/', include('task_06.urls')),
    path('task_08/', include('task_08.urls')),
    path('task_09/', include('task_09.urls')),
    path('task_10/', include('task_10.urls')),
]