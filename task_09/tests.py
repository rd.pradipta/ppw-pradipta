from django.urls import resolve
from django.test import TestCase, Client
from .urls import app_name

class Task09Test(TestCase):
    
    def test_task06_check_name(self):
        self.assertEqual(app_name,"task_09")

    def test_task_09_url_exist(self):
        response = Client().get('/task_09/')
        self.assertEqual(response.status_code,200)
    
    def test_task_09_using_template(self):
        response = Client().get('/task_09/')
        self.assertTemplateUsed(response, 'movies.html')