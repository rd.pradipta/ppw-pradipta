from django.urls import path
from task_09.views import seeTask09, findBook

app_name = "task_09"

urlpatterns = [
	path('', seeTask09.as_view()),
	path('find-book/', findBook.as_view(), name='find-book'),
]