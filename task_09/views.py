import json
import requests

from .forms import find_books

from django.views import View
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt



class findBook(View):

    @csrf_exempt
    def post(self, request):
        json_req = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
        json_data = json_req.json()
        key = request.POST.get('book_to_find', None)
        
        found_books = {
            'books' : []
        }
        for book in json_data['items']:
            if key in book['volumeInfo']['title']:
                found_books['books'].append(book['volumeInfo']['title'])
            else:
                continue
        return JsonResponse(found_books)

class seeTask09(View):
    template_file = "movies.html"
    def get(self,request):
        json_req = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
        json_data = json_req.json()
        all_books = {
            'books' : [],
        }
        response = {
            'book_items' : all_books,
            'liked_books' : 0,
            'search_bar' :  find_books(),
            'description' : "Here you can find hundreds of books that you might love! \
                            Click buttons below to get started",
            'title' : "Welcome to Dipsi Library",
        }
        for book in json_data['items']:
            current_book = {
                'title' : '',
                'publisher' : '',
                'image_link' : '',
                'description' : '',
                'authors' : [],
            }
            if 'title' in book['volumeInfo']:
                current_book['title'] = book['volumeInfo']['title']
            if 'publisher' in book['volumeInfo']:
                current_book['publisher'] = book['volumeInfo']['publisher']
            if 'imageLinks' in book['volumeInfo']:
                current_book['image_link'] = book['volumeInfo']['imageLinks']['thumbnail']
            if 'description' in book['volumeInfo']:
                current_book['description'] = book['volumeInfo']['description']
            if 'authors' in book['volumeInfo']:
                current_book['authors'] = ', '.join([author for author in book['volumeInfo']['authors']])            
            
            all_books['books'].append(current_book)
        return render(request, self.template_file, response)