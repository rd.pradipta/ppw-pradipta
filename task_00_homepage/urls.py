from django.urls import path
from . import views

app_name = "task_00_homepage"

urlpatterns = [
    path('', views.home, name="home_page"),
]
