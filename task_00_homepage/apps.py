from django.apps import AppConfig


class Task00HomepageConfig(AppConfig):
    name = 'task_00_homepage'
