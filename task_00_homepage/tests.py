from django.test import TestCase
from django.http import HttpRequest
from .views import home


class HomePageUnitTest(TestCase):

    # Check URL Routing
    def page_is_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def page_is_using_home_func(self):
        found = resolve('')
        self.assertEqual(found.func, home)

    def page_is_using_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'homepage.html')


    # def page_function_sum(self):
    #     self.assertTrue(len(home) >= 1)

    # def page_is_written(self):
    #     self.assertIsNone(landing_page_content)

    # def page_is_complete(self):
    #     request = HttpRequest()
    #     response = index(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn(landing_page_content, html_response)

    # def page_content_check(self):
    #     self.assertTrue(len(landing_page_content) >= 1)