// Bagi siapapun yang sedang membaca, aku tidak menggunakan syntax AJAX asli, melainkan
// shorthand dengan menggunakan JQuery

// Variabel global
var username_taken = false;
var email_taken = false;

$(document).ready(function(){
    M.AutoInit();
    secureAJAX();

// Function to validate username
    $('#id_user_name').change(function(){
        var name = $("#id_user_name").val();
        username_taken = false;

        // Shorthand AJAX dengan JQuery
        $.post( "validate-username/", {
            "username_to_check": name
        },"json")
            // Pemanggilan AJAX sukses
            .done(function(data) {
                // Jika username telah terdaftar
                if(data.already_registered){
                    username_taken = true;
                    pushError("Sorry, This username is already taken, Please use another username!");
                    setTimeout(function(){
                        $("#errorCard").addClass("hide");
                    }, 5000);
                };
                validate_data();
            })
            // Pemanggilan AJX gagal, umumnya karena masalah sistem & internet
            .fail(function(data){
                AJAXFail(data);
        });
    });

    // Function to validate email
    $('#id_user_email').change(function(){
        var email = $('#id_user_email').val();
        email_taken = false;

        // Shorthand AJAX using JQuery functions
        $.post( "validate-email/", {
            "email_to_check": email
        },"json")
            // Pemanggilan AJAX sukses
            .done(function(data) {
                // Jika email telah terdaftar
                if(data.already_registered){
                    email_taken = true;
                    pushError("Sorry, This e-mail already taken, Please use another e-mail address!");
                    setTimeout(function(){
                        $("#errorCard").addClass("hide");
                    }, 5000);
                };
                validate_data();
            })
            // Pemanggilan AJAX gagal, umumnya karena masalah sistem & internet
            .fail(function(data){
                AJAXFail(data);
        });
    });

    $('#id_first_name').change( function(){
        validate_data();
    });

    $('#id_user_password').change( function(){
        validate_data();
    });

});

function secureAJAX(){
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
}

function csrfSafeMethod(method) { return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method)); }

function validate_data(){
    // Cek apakah username dan email udah taken ? Pastikan keduanya TIDAK terdaftar (false)
    // Cek apakah nama dan password ada isinya, kalau valid, button berubah, jika tidak maka biarkan dalam state tidak aktif
    var username = $('#id_user_name').val();
    var password = $('#id_user_password').val();
    var email = $('#id_user_email').val();
    var name = $('#id_first_name').val();

    if(username !== '' && !username_taken && password !== '' && email !== '' && !email_taken && name !== ''){ activate_button(); }
    else{ deactivate_button(); }
}

function register_user(){

    var username = $('#id_user_name').val();
    var password = $('#id_user_password').val();
    var email = $('#id_user_email').val();
    var first_name = $('#id_first_name').val();
    var last_name = $('#id_last_name').val();

    $.post( "start-register/", {
        "username_to_send": username,
        "password_to_send": password,
        "email_to_send": email,
        "first_name_to_send": first_name,
        "last_name_to_send": last_name,
    },"json")
        // Pemanggilan AJAX sukses
        .done(function(data) {
            let result = (data.registered_status) ? pushSuccess("Registration Successful!") : pushError("Registration Failed!");
        })
        // Pemanggilan AJAX gagal, umumnya karena masalah sistem & internet
        .fail(function(data){
            AJAXFail(data);
        });
}

function activate_button(){
    if ( $("#register_btn").hasClass("grey") ){
        $("#register_btn").removeClass("grey").addClass("red");
        $("#register_btn").attr("onclick","register_user()");
    }
}
function deactivate_button(){
    if ( $("#register_btn").hasClass("red") ){
        $("#register_btn").removeClass("red").addClass("grey");
        $("#register_btn").attr("onclick","");
    }
}

function pushError(msg){
    $("#successCard").addClass("hide");
    $("#errorCard").removeClass("hide");
    $("#error").text(msg);
}

function pushSuccess(msg){
    $("#errorCard").addClass("hide");
    $("#successCard").removeClass("hide");
    $("#success").text(msg);
}

function AJAXFail(data){
    pushError("An error occured, Please check your internet connection & try again!");
    setTimeout(function(){
        $("#errorCard").addClass("hide");
    }, 5000);
}