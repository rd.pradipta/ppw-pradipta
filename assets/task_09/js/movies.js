$(document).ready(function(){
    M.AutoInit();
});

function likeBook(bookID){
    if( $("#" + bookID).attr("value") != "Added" ){ add_like( $("#" + bookID), $("#book-counter") ) }
    else { remove_like( $("#" + bookID), $("#book-counter") ); }
}

function reveal_books(){
    if ($('#availabe_books').hasClass("hide")){
        $('#availabe_books').removeClass("hide");
        $('#reveal_book_btn').removeClass("green").addClass("red");
        $('#reveal_book_btn').html("Hide Availabe Books");
    }
    else{
        $('#availabe_books').addClass("hide");
        $('#reveal_book_btn').addClass("green").removeClass("red");
        $('#reveal_book_btn').html("Show Availabe Books");
    }
}

function add_like(bookID, bookCounter){
    if (bookID.hasClass("orange")){
        bookID.removeClass("orange").addClass("grey");
        bookID.html("LIKED");
    }
    bookCounter.html(parseInt(bookCounter.html()) + 1);
    bookID.attr("value","Added");
}

function remove_like(bookID, bookCounter){
    if (bookID.hasClass("grey")){
        bookID.removeClass("grey").addClass("orange");
        bookID.html("LIKE");
    }
    bookCounter.html(parseInt(bookCounter.html()) - 1);
    bookID.attr("value","notAdded");
}

function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function find_books(){
    var key = $("#id_book_name").val();
    if(key.length == 0){
        pushError("Please enter keyword before continue!");
        setTimeout(function(){
            $("#errorCard").addClass("hide");
        }, 5000);
    }
    else{
        var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });
        $("#loadingCard").removeClass("hide");
        $.ajax({
            method: "POST",
            url: 'https://ppw-a-dipsi.herokuapp.com/task_09/find-book/',
            data : {
                "book_to_find" : key
            },
            dataType: "json",
            success : function(data){
                $("#loadingCard").addClass("hide");
                if(data.books.length == 0){
                    pushError("Sorry, Book not found!");
                    setTimeout(function(){
                        $("#errorCard").addClass("hide");
                    }, 5000);
                }
                else{
                    var result = "We found some books!";
                    var all_books = "";
                    pushSuccess(result);
                    for (i = 0; i < data.books.length; i++) {
                        all_books += "<td>" + data.books[i] + "</td><br>"
                    }
                    $("#books-found-table").removeClass("hide");
                    $("#books-data").html(all_books);
                };
            },
            error : function(data){
                $("#loadingCard").addClass("hide");
                pushError("An error occured, Please check your connection and try again!");
            },
        });
    }
}

function pushError(msg){
    $("#successCard").addClass("hide");
    $("#errorCard").removeClass("hide");
    $("#error").text(msg);
}

function pushSuccess(msg){
    $("#errorCard").addClass("hide");
    $("#successCard").removeClass("hide");
    $("#success").text(msg);
}