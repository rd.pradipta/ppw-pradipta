$(document).ready(function(){
    init();
});

function init(){
    M.AutoInit();
}

function lihatTentang(){
    $("#section-education").addClass("hide");
    $("#section-experiences").addClass("hide");
    $("#section-skills").addClass("hide");   
}

function lihatPendidikan(){
    $("#section-education").removeClass("hide");
    $("#section-experiences").addClass("hide");
    $("#section-skills").addClass("hide"); 
}

function lihatPengalaman(){
    $("#section-education").addClass("hide");
    $("#section-experiences").removeClass("hide");
    $("#section-skills").addClass("hide");   
}

function lihatKeahlian(){
    $("#section-education").addClass("hide");
    $("#section-experiences").addClass("hide");
    $("#section-skills").removeClass("hide");   
}