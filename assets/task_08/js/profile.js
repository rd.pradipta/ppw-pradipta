$(document).ready(function(){
    init();
});

function init(){
    $("body").css("background-image", "url('/static/task_08/images/bg_loader.jpg')");
    setTimeout(showPage, 5000);
}

function showPage() {
    $("#preloader").addClass("hide");
    $("#all-content").removeClass("hide");
    M.AutoInit();
    $("body").css("background-image", "url('/static/task_08/images/bg_default.jpg')");
}

function seeAbout(){
    $("#section-education").addClass("hide");
    $("#section-experiences").addClass("hide");
    $("#section-skills").addClass("hide");   
}

function seeEducation(){
    $("#section-education").removeClass("hide");
    $("#section-experiences").addClass("hide");
    $("#section-skills").addClass("hide"); 
}

function seeExperience(){
    $("#section-education").addClass("hide");
    $("#section-experiences").removeClass("hide");
    $("#section-skills").addClass("hide");   
}

function seeSkill(){
    $("#section-education").addClass("hide");
    $("#section-experiences").addClass("hide");
    $("#section-skills").removeClass("hide");   
}

function defaultTheme(){
    if ($("#navbar").hasClass("red")){
        $("#navbar").removeClass("red").addClass("orange");
        $("#button").removeClass("red").addClass("orange");
    }
    else if ($("#navbar").hasClass("blue")){
        $("#navbar").removeClass("blue").addClass("orange");
        $("#button").removeClass("blue").addClass("orange"); 
    }
    $("body").css("background-image", "url('/static/task_08/images/bg_default.jpg')");
}

function redTheme(){
    if ($("#navbar").hasClass("blue")){
        $("#navbar").removeClass("blue").addClass("red");
        $("#button").removeClass("blue").addClass("red");
    }
    else if ($("#navbar").hasClass("orange")){
        $("#navbar").removeClass("orange").addClass("red");
        $("#button").removeClass("orange").addClass("red"); 
    }
    $("body").css("background-image", "url('/static/task_08/images/bg_red.jpg')");
}

function blueTheme(){
    if ($("#navbar").hasClass("red")){
        $("#navbar").removeClass("red").addClass("blue");
        $("#button").removeClass("red").addClass("blue");
    }
    else if ($("#navbar").hasClass("orange")){
        $("#navbar").removeClass("orange").addClass("blue");
        $("#button").removeClass("orange").addClass("blue"); 
    }
    $("body").css("background-image", "url('/static/task_08/images/bg_blue.jpg')");
}

function selector(){

}