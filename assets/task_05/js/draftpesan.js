$(document).ready(function(){
    init();
});

function init(){
    M.AutoInit();
}

function pushError(msg){
    $("#successCard").addClass("hide");
    $("#errorCard").removeClass("hide");
    $("#error").text(msg);
}

function pushSuccess(msg){
    $("#errorCard").addClass("hide");
    $("#successCard").removeClass("hide");
    $("#success").text(msg);
}

function checkValidation(sender,receiver,urgency,description){
    if (sender == "" || receiver == "" || urgency == "" || description == ""){
        throw new Error("Mohon Semua Kolomnya di Isi Ya!");
    }
    else{
        return true;
    }
}