import datetime
from django.db import models
from django.utils import timezone

class StatusContent(models.Model):
    status_name = models.CharField(max_length=200)
    status_content = models.TextField(max_length=300)
    status_date = models.DateTimeField()