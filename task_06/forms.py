from django import forms

class StatusForm(forms.Form):
    status_name = forms.CharField(max_length=200, required=True, label='Nama Kamu')
    status_content = forms.CharField(max_length=300, required=True, label='Status Kamu')