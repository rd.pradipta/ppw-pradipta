from django.urls import resolve
from django.test import TestCase, Client
from django.utils import timezone

from .models import StatusContent
from .views import queryToJSON, seeTask06, addStatus
from .urls import app_name

class Task06Test(TestCase):

    STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

    def test_task06_check_name(self):
        self.assertEqual(app_name,"task_06")

    def test_task06_url_exist(self):
        response = Client().get('/task_06/')
        self.assertEqual(response.status_code,200)

    def test_task06_using_function(self):
        found = resolve('/task_06/')
        self.assertEqual(found.func, seeTask06)

    def test_task06_using_template(self):
        response = Client().get('/task_06/')
        self.assertTemplateUsed(response, 'hello.html')

    def test_task06_has_addStatus_function(self):
        found = resolve('/task_06/add_status/')
        self.assertEqual(found.func, addStatus)

    def test_task06_test_object(self):
        data = StatusContent.objects.create(status_name='Dipsi',
                                            status_content='I need Sleep',
                                            status_date=timezone.now())
        count_data = StatusContent.objects.all().count()
        self.assertEqual(count_data,1)

    def test_task_06_create_POST_request(self):
        response = Client().post( '/task_06/add_status/',
                                    data={  'status_name':'Pradipta Gitaya',
                                            'status_content':'I need Food and Money',
                                            'status_date':'2018-10-11T23:55'
                                         }
                                    )                   
        data = StatusContent.objects.all()
        self.assertEqual(data.count(),1)
        self.assertNotEqual(len(queryToJSON(data.values())),0)
        self.assertEqual(response.status_code,302)