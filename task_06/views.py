import pytz
import json

from datetime import datetime
from django.http import HttpResponse
from django.shortcuts import render, redirect

from .forms import StatusForm
from .models import StatusContent

status_dict = {}

def queryToJSON(queryset):
    result = []
    for data in queryset:
        result.append(data)
    return result

def seeTask06(request):
    form = StatusForm()
    status_dict = StatusContent.objects.all().values()
    status_data = queryToJSON(status_dict)
    response = {  'form_status' : form,
                  'semua_status' : status_data }
    return render(  request, "hello.html", response)

def addStatus(request):
    form = StatusForm(request.POST)
    if request.method == "POST" and form.is_valid():
        name = request.POST['status_name']
        content = request.POST['status_content']
        date = datetime.strptime(request.POST['status_date'],'%Y-%m-%dT%H:%M')
        StatusContent.objects.create(   status_name=name,
                                        status_content=content,
                                        status_date=date.replace(tzinfo=pytz.UTC))
        return redirect("/task_06/")