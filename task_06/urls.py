from django.urls import path
from . import views

app_name = "task_06"

urlpatterns = [
    path('', views.seeTask06, name="task_06"),
    path('add_status/', views.addStatus, name="add_status"),
]