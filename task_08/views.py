from django.views import View
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

navigation_content = []
description = \
"""
A passionate learner.
Interested in web & apps development.
Currently enrolled as second year student of Faculty of Computer Science, Universitas Indonesia.
Majoring Information System & Technology.
"""
contact = ["21 Years Old", "Passionate Learner", "Jakarta, Indonesia", "(+62) 81218466075"]

class seeTask08(View):
    template_file = "profile.html"
    def get(self, request):
        response = {
            "navigation_content" : navigation_content,
            "name" : "Rd Pradipta Gitaya S",
            "description" : description,
            "contact" : contact,
        }     
        return render(request, self.template_file, response)
    
    def post(self, request):
        pass

class Navigation:
    def __init__(self,name,icon_label,link):
        self.name = name
        self.icon_label = icon_label
        self.link = link

navigation_content.append(Navigation("About","person","seeAbout()"))
navigation_content.append(Navigation("Education","school","seeEducation()"))
navigation_content.append(Navigation("Experiences","work","seeExperience()"))
navigation_content.append(Navigation("Skills","timelapse","seeSkill()"))
