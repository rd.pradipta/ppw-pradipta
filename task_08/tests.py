from django.urls import resolve
from django.test import TestCase, Client
from .urls import app_name

class Task08Test(TestCase):
    
    def test_task08_check_name(self):
        self.assertEqual(app_name,"task_08")

    def test_task_08_url_exist(self):
        response = Client().get('/task_08/')
        self.assertEqual(response.status_code,200)
    
    def test_task_08_using_template(self):
        response = Client().get('/task_08/')
        self.assertTemplateUsed(response, 'profile.html')