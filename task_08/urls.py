from django.urls import path
from task_08.views import seeTask08

app_name = "task_08"

urlpatterns = [
	path('', seeTask08.as_view(), name="task_08"),
]