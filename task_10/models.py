import datetime
from django.db import models
from django.utils import timezone

class registeredSubscriber(models.Model):

    user_name = models.CharField(max_length=50)
    user_password = models.CharField(max_length=50)
    user_email = models.CharField(max_length=100)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    
    def __str__(self):
        return  "Username: " + self.user_name + " " + \
                "Password: " + self.user_password + " " + \
                "Email: " + self.user_email + " " + \
                "First Name: " + self.first_name + " " + \
                "Last Name: " + self.last_name