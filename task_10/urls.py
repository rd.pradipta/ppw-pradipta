from django.urls import path
from task_08.views import seeTask08
from task_10.views import Index, ValidateUsername, ValidateEmail, Registration

app_name = "task_10"

urlpatterns = [
	path('', seeTask08.as_view(), name="task_08"),
	path('registration/', Index.as_view(), name='registration-page'),
	path('registration/validate-username/', ValidateUsername.as_view(), name='validate-username'),
	path('registration/validate-email/', ValidateEmail.as_view(), name='validate-email'),
	path('registration/start-register/', Registration.as_view(), name='start-register'),
]