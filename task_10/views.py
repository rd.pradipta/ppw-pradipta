import json
import requests

from django.views import View
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from .forms import SubscriberRegistrationForm
from .models import registeredSubscriber

# Ini adalah class based views, lebih nyaman dipakai untuk saya
button_enabler_counter = 0
class Index(View):
    template_file = "profile_and_subscribers.html"

    def get(self, request):
        registration_form = SubscriberRegistrationForm()
        registered_users = registeredSubscriber.objects.all()

        response = {
            "title" : "Register Now!",
            "description" : "You'll get latest news about me!",
            "registration_form" : registration_form,
            "registered_users" : registered_users,
        }     
        return render(request, self.template_file, response)

class ValidateUsername(View):
    @csrf_exempt
    def post(self, request):
        username_to_check = request.POST.get('username_to_check', None)
        data = {
            "already_registered" : registeredSubscriber.objects.filter(user_name = username_to_check).exists()
        }
        return JsonResponse(data)

class ValidateEmail(View):
    @csrf_exempt
    def post(self, request):
        email_to_check = request.POST.get('email_to_check', None)
        data = {
            "already_registered" : registeredSubscriber.objects.filter(user_email = email_to_check).exists()
        }
        return JsonResponse(data)

class Registration(View):
    @csrf_exempt
    def post(self, request):
        username = request.POST['username_to_send']
        password = request.POST['password_to_send']
        email = request.POST['email_to_send']
        fname = request.POST['first_name_to_send']
        lname = request.POST['last_name_to_send']
        person = registeredSubscriber(  user_name=username, user_password=password,
                                        user_email=email, first_name=fname,
                                        last_name=lname)
        person.save()
        data = {
            "registered_status" : True
        }
        return JsonResponse(data);