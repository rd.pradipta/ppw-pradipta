from django import forms

class SubscriberRegistrationForm(forms.Form):
    user_name = forms.CharField(max_length=50, required=True, label='Username')
    user_password = forms.CharField(max_length=50, required=True, label='Password', widget=forms.PasswordInput)
    user_email = forms.CharField(max_length=100, required=True, label='Email Address')
    first_name = forms.CharField(max_length=200, required=True, label='First Name')
    last_name = forms.CharField(max_length=200, label='Last Name')